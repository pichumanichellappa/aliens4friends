# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2021 NOI Techpark <p.moser@noi.bz.it>

FROM openjdk:11-jre-slim

ARG GITHUB_BOT_AUTH=

# hadolint ignore=DL3008
RUN apt-get update && apt-get -y install --no-install-recommends openjdk-11-jre-headless wget && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives

RUN wget --progress=dot:giga -P /usr/local/lib \
    https://${GITHUB_BOT_AUTH}@github.com/spdx/tools/releases/download/v2.2.5/spdx-tools-2.2.5-jar-with-dependencies.jar

CMD [ "java",  "-jar", "/usr/local/lib/spdx-tools-2.2.5-jar-with-dependencies.jar" ]
