# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

import os
import re
import logging

from aliens4friends.commons.settings import Settings
from aliens4friends.commons.pool import FILETYPE
from aliens4friends.commands.command import Command, CommandError, Processing
from aliens4friends.models.tinfoilhat import TinfoilHatModel
from aliens4friends.commons.utils import bash

logger = logging.getLogger(__name__)

class GenSBoM(Command):
	"""collect all final spdx files filtered by tinfoilhat tags, and
	copy them in a single folder"""

	def __init__(self, session_id: str, regex: str, target_dir: str, dryrun: bool):
		super().__init__(session_id, Processing.MULTI, dryrun)
		self.regex = re.compile(regex)
		self.target_dir = target_dir
		self.dryrun = dryrun

	def run(self, path: str):
		tfh = TinfoilHatModel.from_file(path)
		found = False
		for recipe_name, container in tfh._container.items():
			for tag in container.tags:
				if self.regex.match(tag):
					found = True
					break
			if found:
				break
		if found:
			name, version, variant, _, _ = self.pool.packageinfo_from_path(path)
			cur_pckg = f"{name}-{version}-{variant}"
			cur_path = self.pool.abspath(
				Settings.PATH_USR,
				name,
				version
			)
			logger.info(f"[{cur_pckg}] tag '{tag}' matches regex")
			spdx_file = os.path.join(cur_path, f'{cur_pckg}.final.spdx')
			logger.debug(f"[{cur_pckg}] searching for {spdx_file}")
			if os.path.isfile(spdx_file):
				logger.info(f"[{cur_pckg}] copying final SPDX file")
				bash(f"cp {spdx_file} {self.target_dir}")
			else:
				raise CommandError(f"[{cur_pckg}] unable to find final SPDX file")
		return True

	@staticmethod
	def execute(
		regex: str,
		target_dir: str,
		session_id: str = "",
		dryrun: bool = False
	) -> bool:
		if not os.path.isdir(target_dir):
			bash(f"mkdir -p {target_dir}")
		cmd = GenSBoM(session_id, regex, target_dir, dryrun)
		return cmd.exec_with_paths(FILETYPE.TINFOILHAT)
